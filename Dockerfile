FROM python:3.12-slim

ARG DB_Y_M=2024-10

WORKDIR /app
COPY . ./

# Add IP to Country Lite DB (updated manually)
# You have to add ip_db_path = /app/... in your config.ini
# https://db-ip.com/db/download/ip-to-country-lite
ADD https://download.db-ip.com/free/dbip-country-lite-${DB_Y_M}.mmdb.gz /app
ENV DEBIAN_FRONTEND=noninteractive
RUN gunzip dbip-country-lite-${DB_Y_M}.mmdb.gz && \
    mv dbip-country-lite-${DB_Y_M}.mmdb dbip-country-lite.mmdb && \
    apt update && \
    apt upgrade -y && \
    apt install -y build-essential python3-pip && \
    pip install . && \
    apt remove -y build-essential && \
    apt autoremove -y && \
    apt autoclean -y && \
    rm -rf /var/lib/apt/lists

ENTRYPOINT ["parsedmarc"]
